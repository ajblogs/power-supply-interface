/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "Actuators/st7789v.h"

#define X_PIX_STATIC 130 // Value based on observation
#define Y_PIX_STATIC 28 // Value based on observation

/*Function to display Voltage,Current and Power*/
void display_logo(font_struct *font){
    char *container = "Anuj Falcon !";
    lcd_text(5,Y_PIX_STATIC+10,container,font);
}
/*Function to display Voltage,Current and Power*/
void display_values(float V, color Vc,float A, color Ac,float P, color Pc, font_struct *font){

    // lcd_rect(0x00ff00,10,0,97,33,font->lcd);
    // lcd_rect(0x00ff00,133,0,97,33,font->lcd);
    if(font->font_back_rgb24 == -1) font->font_back_rgb24 = font->lcd->background;

    char container[11]={};
    sprintf(container,"%06.3f V",V);
    font->font_rgb24 = Vc;
    int w = lcd_text(7,2,container,font);

    sprintf(container,"%06.3f A",A);
    font->font_rgb24 = Ac;
    lcd_text(w+26,2,container,font);

    sprintf(container,"%06.3f W",P);
    font->font_rgb24 = Pc;
    w = lcd_text(2,32,container,font);
}
//Local gloabl variable for holding maximum allowed values
static float user_max_v = MAX_VOLTAGE_TO_SENSE;
static float user_max_i = MAX_CURRENT_TO_SENSE;
static float user_max_w = MAX_POWER_TO_SENSE;
static bool safety = true;
/*Function to display absolute max values of Voltage,Current and Power in the right-down corner*/
void display_absmax_values(color Vc, color Ac, color Pc, font_struct *font){
    // sprintf(container,"ABSOLUTE MAXIMUM!");
    lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font->lcd);

    char *c = "ABSOLUTE MAX";
    font->font_rgb24 = 0xFF0000;
    lcd_text(X_PIX_STATIC+3,Y_PIX_STATIC,c,font);

    char container[11]={};
    sprintf(container,"|%04.1f|",(float)user_max_v);
    font->font_rgb24 = Vc;
    int w = lcd_text(X_PIX_STATIC,Y_PIX_STATIC+15,container,font);

    sprintf(container,"%04.1f",(float)user_max_i);
    font->font_rgb24 = Ac;
    w = lcd_text(w,Y_PIX_STATIC+15,container,font);

    sprintf(container,"|%04.1f|",(float)user_max_w);
    font->font_rgb24 = Pc;
    lcd_text(w,Y_PIX_STATIC+15,container,font);
}

/*Power off display*/
void display_PSU_off(st7789v_struct *st7789v_input){
        char *c = "PWR OFF";
        font_struct font = *st7789v_input->font;
        font.font_rgb24 = 0xFF0000;
        lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font.lcd);
        lcd_text(X_PIX_STATIC-3,Y_PIX_STATIC+4,c,&font);
}

/*Power off display*/
void display_safety_off(st7789v_struct *st7789v_input){
        char *c = "NO SAFE";
        font_struct font = *st7789v_input->font;
        font.font_rgb24 = 0xFFFFFF;
        font.font_back_rgb24 = 0xFF0000;
        lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font.lcd);
        lcd_text(X_PIX_STATIC-3,Y_PIX_STATIC+4,c,&font);
}

/*primary menus, menus, options  - text array*/
#define prim_menus_l 7
static char *options[prim_menus_l][3][3] = {
    {
        {"------------","0"}},
    {
        {"Freeze !","1"},
        {"Unfreeze","0"}
    },
    {
        {"cfg Amp","2"},
        {"absMax +","1","+0.1"} ,
        {"absMax -","1","-0.1"}
    },
    {
        {"cfg Volt","2"},
        {"absMax +","1","+0.1"},
        {"absMax -","1","-0.1"}
    },
    {
        {"cfg Watt","2"},
        {"absMax +","1","+0.1"},
        {"absMax -","1","-0.1"}
    },
    {
        {"RESET","2"},
#ifdef SAFETY_NET_OVERRIDE_MENU
        {"UNSAFE","0"},
#else
        {"NO SUP","0"},
#endif
        {"RST SYS","0"}

    },
    {
        {"MAXABS","0"}
    }

};
#define MAXABS 6

bool POWER = false;
static volatile bool st7789v_readiness = true;
/*Clear the menu area and place the default value (Right - down corner above graph)*/
void reset_menu_in_display(st7789v_struct *st7789v_input){
    merge_graph_struct *graph_settings = st7789v_input->merge_graph_settings;
    font_struct font16 = {chr_hgt_f16,max_width_f16,firstchr_f16,widtbl_f16,chrtbl_f16,font16_decoder,0x0,st7789v_input->font->lcd->background,st7789v_input->font->lcd};
    if(!safety && POWER) display_safety_off(st7789v_input);
    else if(POWER) display_absmax_values(graph_settings->plot_V_color,graph_settings->plot_A_color,graph_settings->plot_P_color,&font16);
    else display_PSU_off(st7789v_input);
}
void reset_menu(st7789v_struct *st7789v_input){
    const xTaskHandle ina219_task = *st7789v_input->ina219_task;
    const xTaskHandle hmi_task = *st7789v_input->hmi_task;    
    hmi_struct *hmi_data = st7789v_input->hmi_data;
    reset_menu_in_display(st7789v_input);
    xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
    xTaskNotify(ina219_task,ST7789V_NOTIFY_AVAIL,eSetValueWithoutOverwrite);
    st7789v_readiness = true;
    hmi_data->primary_menu = 0;
    hmi_data->menu = 0;
    hmi_data->option = 0;
    hmi_data->status = BLANK;
}

/*Function to send the update maximum I value from user to INA*/
void update_max_I(xTaskHandle ina219_task){
    bool sent = false;
    uint32_t notification = 0;
    uint32_t max_i_decoded = ((uint16_t)((float)user_max_i*(float)100));
    max_i_decoded <<=16;
    ESP_LOGI(ST7789V_TAG,"The user max I: %f while val: %x",user_max_i,max_i_decoded);
    while(!sent) {
        if(xTaskNotify(ina219_task,max_i_decoded|ST7789V_NOTIFY_NEW_MAX_A,eSetValueWithoutOverwrite)){
            vTaskDelay(25/portTICK_PERIOD_MS);
            if(xTaskNotifyWait(0x0,0x0,&notification, 0)){
                if(notification == INA219_ACKNOWLEDGED) sent=true;
                else if(notification == INA219_ACKNOWLEDGED) sent=true;
                else if(notification != INA219_ACKNOWLEDGED) ESP_LOGE(ST7789V_TAG,"Improper response while updating Imax %x",notification);
            }
        }
    }
}
/*Function to send the update maximum V value from user to INA*/
void update_max_V(xTaskHandle ina219_task){
    bool sent = false;
    uint32_t notification = 0;
    uint32_t max_v_decoded = ((uint16_t)((float)user_max_v*(float)100));
    max_v_decoded <<=16;
    ESP_LOGI(ST7789V_TAG,"The user max V: %f while val: %x",user_max_v,max_v_decoded);
    while(!sent) {
        if(xTaskNotify(ina219_task,max_v_decoded|ST7789V_NOTIFY_NEW_MAX_V,eSetValueWithoutOverwrite)){
            vTaskDelay(25/portTICK_PERIOD_MS);
            if(xTaskNotifyWait(0x0,0x0,&notification, 0)){
                if(notification == INA219_ACKNOWLEDGED) sent=true;
                else if(notification != INA219_ACKNOWLEDGED) ESP_LOGE(ST7789V_TAG,"Improper response while updating Vmax %x",notification);
            }
        }
    }
}
/*Function to send the update maximum W value from user to INA*/
void update_max_W(xTaskHandle ina219_task){
    bool sent = false;
    uint32_t notification = 0;
    uint32_t max_w_decoded = ((uint16_t)((float)user_max_w*(float)100));
    max_w_decoded <<=16;
    ESP_LOGI(ST7789V_TAG,"The user max w: %f while val: %x",user_max_w,max_w_decoded);
    while(!sent) {
        if(xTaskNotify(ina219_task,max_w_decoded|ST7789V_NOTIFY_NEW_MAX_W,eSetValueWithoutOverwrite)){
            vTaskDelay(25/portTICK_PERIOD_MS);
            if(xTaskNotifyWait(0x0,0x0,&notification, 0)){
                if(notification == INA219_ACKNOWLEDGED) sent=true;
                else if(notification != INA219_ACKNOWLEDGED) ESP_LOGE(ST7789V_TAG,"Improper response  while updating Wmax %x",notification);
            }
        }
    }
}
/*Supporting function for menu - MAXABS*/
void max_all_abs_limits(st7789v_struct *st7789v_input){
    user_max_w = INA219_MAX_SENSE_POWER;
    user_max_v = INA219_MAX_SENSE_VOLTAGE;
    user_max_i = INA219_MAX_SENSE_CURRENT;
    xTaskHandle ina219_task = *st7789v_input->ina219_task;
    update_max_I(ina219_task);
    update_max_V(ina219_task);
    update_max_W(ina219_task);
    reset_menu(st7789v_input);
}

static char *prim_menu_c;
static uint8_t prim_menu_idx;
/*Function to display primary menus on button press*/
void display_primary_menus(st7789v_struct *st7789v_input){
    prim_menu_idx = (st7789v_input->hmi_data->primary_menu)%prim_menus_l;
    prim_menu_c = options[prim_menu_idx][0][0];
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFF0000;
    lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font.lcd);
    lcd_text(X_PIX_STATIC,Y_PIX_STATIC+4,prim_menu_c,&font);
}
static uint8_t menus_l = 0;
/*Function to let know the selection and execute if necessary based on the selected primary-menu*/
void display_primary_menu_selected(st7789v_struct *st7789v_input){
    if(prim_menu_idx == 0) reset_menu(st7789v_input);
    // else if(prim_menu_idx == 5) esp_restart();
    else if(prim_menu_idx == 6) max_all_abs_limits(st7789v_input);
    else{
        menus_l = (int)*options[prim_menu_idx][0][1] - 0x30;
        font_struct font = *st7789v_input->font;
        font.font_rgb24 = 0xFFCC00;
        lcd_text(X_PIX_STATIC,Y_PIX_STATIC+4,prim_menu_c,&font);
    }
}

static char *menu_c;
static uint8_t menu_idx;
/*Function to display menus on button press*/
void display_menus(st7789v_struct *st7789v_input){
    menu_idx = (st7789v_input->hmi_data->menu%menus_l)+1;
    menu_c = options[prim_menu_idx][menu_idx][0];
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFF0000;
    lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font.lcd);
    lcd_text(X_PIX_STATIC,Y_PIX_STATIC+4,menu_c,&font);
}
static uint8_t options_l = 0;
/*Function to let know the selection and execute if necessary based on the selected menu*/
void display_menu_selected(st7789v_struct *st7789v_input){
    if(prim_menu_idx == 1) reset_menu(st7789v_input);
    else if(prim_menu_idx == 5 && menu_idx == 2) esp_restart();
    else if(prim_menu_idx == 5 && menu_idx == 1){ 
        #ifdef SAFETY_NET_OVERRIDE_MENU
            xTaskNotify(*st7789v_input->ina219_task,INA219_SAFETY_NET_DISABLE_TOGGLE,eSetValueWithoutOverwrite);
            safety = !safety;
        #endif
        reset_menu(st7789v_input);
    }
    else{
        options_l = (int)*options[prim_menu_idx][menu_idx][1] - 0x30;
        font_struct font = *st7789v_input->font;
        font.font_rgb24 = 0xFFCC00;
        lcd_text(X_PIX_STATIC,Y_PIX_STATIC+4,menu_c,&font);
    }
}

static char *option_c;
static uint8_t option_idx;
#define menu_v_inc 0.5
#define menu_i_inc 0.1
#define menu_w_inc 0.2
/*Function to display options on button press*/
void display_options(st7789v_struct *st7789v_input){
    option_idx = (st7789v_input->hmi_data->option%options_l)+2;
    option_c = options[prim_menu_idx][menu_idx][option_idx];
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFF0000;
//Decode and update the menu (functionally)
    int v=0,i=0,w=0;
    char option_rtc[10];
    if(prim_menu_idx == 2){
        i++;
        if(menu_idx == 1){ 
            user_max_i += ((float)menu_i_inc*(float)i);             
        }
        if(menu_idx == 2){ 
            user_max_i -= ((float)menu_i_inc*(float)i);             
        }
        if(user_max_i>INA219_MAX_SENSE_CURRENT) user_max_i = INA219_MAX_SENSE_CURRENT;
        else if(user_max_i<INA219_MIN_SENSE_CURRENT) user_max_i = INA219_MIN_SENSE_CURRENT;
        sprintf(option_rtc,"%05.2fA",user_max_i);
    }else if(prim_menu_idx == 3){
        v++;
        if(menu_idx == 1){ 
            user_max_v += ((float)menu_v_inc*(float)v);             
        }
        if(menu_idx == 2){ 
            user_max_v -= ((float)menu_v_inc*(float)v);             
        }
        if(user_max_v>INA219_MAX_SENSE_VOLTAGE) user_max_v = INA219_MAX_SENSE_VOLTAGE;
        else if(user_max_v<INA219_MIN_SENSE_VOLTAGE) user_max_v = INA219_MIN_SENSE_VOLTAGE;
        sprintf(option_rtc,"%05.2fV",user_max_v);
    }else if(prim_menu_idx == 4){
        w++;
        if(menu_idx == 1){ 
            user_max_w += (float)menu_w_inc*(float)w;             
        }
        if(menu_idx == 2){ 
            user_max_w -= (float)menu_w_inc*(float)w;             
        }
        if(user_max_w>INA219_MAX_SENSE_POWER) user_max_w = INA219_MAX_SENSE_POWER;
        else if(user_max_w<INA219_MIN_SENSE_POWER) user_max_w = INA219_MIN_SENSE_POWER;
        sprintf(option_rtc,"%05.2fW",user_max_w);
    }
    lcd_rect(0xFFFFFF,X_PIX_STATIC-5,Y_PIX_STATIC,115,40,font.lcd);
    lcd_text(X_PIX_STATIC,Y_PIX_STATIC+4,option_rtc,&font);
}

/*Function to execute based on the selected option*/
void display_option_selected(st7789v_struct *st7789v_input){
    const xTaskHandle ina219_task = *st7789v_input->ina219_task;
    bool w_update = false;
    if(prim_menu_idx == 2){ 
        w_update = true;
        update_max_I(ina219_task);
    }
    else if(prim_menu_idx == 3){
        w_update = true;
        update_max_V(ina219_task);
    }  
    if(prim_menu_idx == 4 || w_update){
        if(w_update){
            user_max_w = user_max_i*user_max_v;
            w_update = false;
        }
        update_max_W(ina219_task);
    }
    reset_menu(st7789v_input);
}

/*Overcurrent alert display*/
void display_OC_alert(st7789v_struct *st7789v_input){
    char c[30];
    sprintf(c,"CURRENT > %05.2fA",(float)user_max_i);
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFFFFFF;
    font.font_back_rgb24 = 0xFF0000;
    lcd_fill(0xFF0000,font.lcd);
    lcd_text(5,Y_PIX_STATIC+50,c,&font);
    display_values(st7789v_input->display_data->V,font.font_rgb24,st7789v_input->display_data->Icalc,font.font_rgb24,st7789v_input->display_data->Pcalc,font.font_rgb24,&font);    
    vTaskDelay(alert_default_display_timeperiod);
}
/*Overvoltage alert display*/
void display_OV_alert(st7789v_struct *st7789v_input){
    char c[30];
    sprintf(c,"VOLTAGE > %05.2fV",(float)user_max_v);
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFFFFFF;
    font.font_back_rgb24 = 0xFF0000;
    lcd_fill(0xFF0000,font.lcd);
    lcd_text(5,Y_PIX_STATIC+50,c,&font);
    display_values(st7789v_input->display_data->V,font.font_rgb24,st7789v_input->display_data->Icalc,font.font_rgb24,st7789v_input->display_data->Pcalc,font.font_rgb24,&font);    
    vTaskDelay(alert_default_display_timeperiod);
}
/*Overpower alert display*/
void display_OW_alert(st7789v_struct *st7789v_input){
    char c[30];
    sprintf(c,"WATTAGE > %05.2fW",(float)user_max_w);
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFFFFFF;
    font.font_back_rgb24 = 0xFF0000;
    lcd_fill(0xFF0000,font.lcd);
    lcd_text(5,Y_PIX_STATIC+50,c,&font);
    display_values(st7789v_input->display_data->V,font.font_rgb24,st7789v_input->display_data->Icalc,font.font_rgb24,st7789v_input->display_data->Pcalc,font.font_rgb24,&font);    
    vTaskDelay(alert_default_display_timeperiod);
}
/*INA219 device disconnection alert display*/
void display_INAdisconnect_error(st7789v_struct * st7789v_input){
    char *c = "INA219 GONE";
    font_struct font = *st7789v_input->font;
    font.font_rgb24 = 0xFFFFFF;
    font.font_back_rgb24 = 0xFF0000;
    lcd_fill(0xFF0000,font.lcd);
    lcd_text(5,Y_PIX_STATIC,c,&font);    
}

static int Mx = 0;
void st7789v_routine(void *st7789v_data_pointer){
    vTaskDelay(100/portTICK_PERIOD_MS); // Wait for all the tasks to initialize
    st7789v_struct *st7789v_input = (st7789v_struct *)st7789v_data_pointer;
    ina219_struct *ina219_input = st7789v_input->display_data;
    const xTaskHandle ina219_task = *st7789v_input->ina219_task;
    const xTaskHandle hmi_task = *st7789v_input->hmi_task;
    merge_graph_struct *graph_settings = st7789v_input->merge_graph_settings;
    color I_rgb16 = rgb24_16(graph_settings->plot_A_color);
    color V_rgb16 = rgb24_16(graph_settings->plot_V_color);
    color P_rgb16 = rgb24_16(graph_settings->plot_P_color);
    int graph_pixel_Y_range;
    if(graph_settings->only_positive) graph_pixel_Y_range = (graph_settings->sheet_height)-1;
    else graph_pixel_Y_range = (graph_settings->sheet_height)/2;
    float y_per_v = (float)graph_pixel_Y_range/(float)MAX_VOLTAGE_TO_SENSE;
    float y_per_a = (float)graph_pixel_Y_range/(float)MAX_CURRENT_TO_SENSE;
    float y_per_p = (float)graph_pixel_Y_range/(float)MAX_POWER_TO_SENSE;
    ESP_LOGW(ST7789V_TAG,"Pix/V:%f Pix/A:%f Pix/W:%f",y_per_v,y_per_a,y_per_p);
    //Display absolute max value
    font_struct font16 = {chr_hgt_f16,max_width_f16,firstchr_f16,widtbl_f16,chrtbl_f16,font16_decoder,0x0,st7789v_input->font->lcd->background,st7789v_input->font->lcd};
    display_logo(&font16); vTaskDelay(1500/portTICK_PERIOD_MS);
    display_absmax_values(graph_settings->plot_V_color,graph_settings->plot_A_color,graph_settings->plot_P_color,&font16);   
    uint32_t incoming_notification = 0;
    uint8_t cnt=0;
    while(1){
        cnt++;if(cnt > 3) cnt = 1;
        incoming_notification = 0;
        ESP_LOGW(ST7789V_TAG,"INCOMING!");
        if(!xTaskNotifyWait(0,0,&incoming_notification,300/portTICK_PERIOD_MS)){
            if(st7789v_readiness){
                ESP_LOGE(ST7789V_TAG,"No display data. Requesting INA219!");
                xTaskNotify(ina219_task,ST7789V_NOTIFY_AVAIL,eSetValueWithoutOverwrite);
            }
        }
        else{
            ESP_LOGW(ST7789V_TAG,"The notification value is %x",incoming_notification);
            if(safety && incoming_notification>>8 == 0x0E){ 
                display_PSU_off(st7789v_input);
                POWER = false;
            }
            if(incoming_notification==INA219_NOTIFY_AVAILABLE_NEW_DATA){
                if(st7789v_readiness == true){    
                    ESP_LOGI(ST7789V_TAG,"volt:%#04x curent:%#04x power:%#04x shunt:%#04x",ina219_input->raw_voltage,ina219_input->raw_current,ina219_input->raw_power,ina219_input->raw_shunt_voltage);
                    ESP_LOGD(ST7789V_TAG,"%fV , %fA , %fW",ina219_input->V,ina219_input->I,ina219_input->P);
                    ESP_LOGD(ST7789V_TAG,"Calc curent:%fA Calc power:%fW",ina219_input->Icalc,ina219_input->Pcalc);
                    
                    //Scaling the values based on maximum to sense values and sheet height
                    int Vy = ((ina219_input->V)*y_per_v)+0.5;
                    int Ay = ((ina219_input->Icalc)*y_per_a)+0.5;
                    int Py = ((ina219_input->Pcalc)*y_per_p)+0.5;
                    ESP_LOGI(ST7789V_TAG,"The calculated values for graph in pixel V:%d A:%d P:%d",Vy,Ay,Py);

                    int input_data[data_length][2]={{Vy,V_rgb16},{Ay,I_rgb16},{Py,P_rgb16}};
                    float V = ina219_input->V;
                    float A = ina219_input->Icalc;
                    float W = ina219_input->Pcalc;

                    //Cross check if negative values peek in to a positive only graph. If yes, zero them.
                    if(graph_settings->only_positive){
                        for(int a=0;a<data_length;a++){
                            if(input_data[a][qty] < 0 ){
                                ESP_LOGE(ST7789V_TAG,"Negative values in POSITIVE only graph!");
                                input_data[a][qty] = 0;
                            }
                        }
                    }
                    if(cnt == 3){
                        if(graph_settings->only_positive){
                            if(V<0) V=0;
                            if(A<0) A=0;
                            if(W<0) W=0;
                        }
                        display_values(V,graph_settings->plot_V_color,A,graph_settings->plot_A_color,W,graph_settings->plot_P_color,st7789v_input->font);
                    }
                    graph_merge_plot(input_data,graph_settings);
                    xTaskNotify(ina219_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                }else xTaskNotify(ina219_task,ST7789V_NOTIFY_BUSY,eSetValueWithOverwrite);
            }
/*Begin unmasked checking of incoming notification*/
            else if(incoming_notification==INA219_OVERCURRENT_ERROR){
                //Display overcurrent banner
                display_OC_alert(st7789v_input);
                while(incoming_notification != INA219_OVERCURRENT_ERROR_HANDLED)
                {
                    display_OC_alert(st7789v_input);
                    xTaskNotifyWait(0,0,&incoming_notification,0);
                }
                xTaskNotify(ina219_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                lcd_fill(st7789v_input->font->lcd->background,st7789v_input->font->lcd);
                reset_menu_in_display(st7789v_input);
                Mx=0;
            }else if(incoming_notification==INA219_OVERVOLTAGE_ERROR){
                //Block all notification until overvoltage error is handled
                display_OV_alert(st7789v_input);
                while(incoming_notification != INA219_OVERVOLTAGE_ERROR_HANDLED){
                    display_OV_alert(st7789v_input);
                    xTaskNotifyWait(0,0,&incoming_notification,0);
                }
                xTaskNotify(ina219_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                lcd_fill(st7789v_input->font->lcd->background,st7789v_input->font->lcd);
                reset_menu_in_display(st7789v_input);
                Mx=0;
            }else if(incoming_notification==INA219_OVERWATT_ERROR){
                //Block all notification until overvoltage error is handled
                while(incoming_notification != INA219_OVERWATT_ERROR_HANDLED){
                    display_OW_alert(st7789v_input);
                    xTaskNotifyWait(0,0,&incoming_notification,0);
                }
                xTaskNotify(ina219_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                lcd_fill(st7789v_input->font->lcd->background,st7789v_input->font->lcd);
                reset_menu_in_display(st7789v_input);
                Mx=0;
            }
            else if((incoming_notification) == INA219_DISCONNECTED){
                display_INAdisconnect_error(st7789v_input);
                while(incoming_notification != INA219_RECONNECTED)
                {xTaskNotifyWait(0,0,&incoming_notification,portMAX_DELAY);}
                xTaskNotify(ina219_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                lcd_fill(st7789v_input->font->lcd->background,st7789v_input->font->lcd);
                reset_menu_in_display(st7789v_input);
                Mx=0;
            }
/*Begin masked checking of incoming notification*/
            else if((incoming_notification & 0x0f)== TOGGLING_PSU_OUT){
                POWER = !POWER;
                xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                reset_menu_in_display(st7789v_input);
            }  // Should come from control
            else if((incoming_notification & 0x0F) == DISPLAY_PRIMARY_MENU) {
                st7789v_readiness = false;
                xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                if((incoming_notification & 0x0f0) == PRESSED) display_primary_menus(st7789v_input);
                else{ 
                    display_primary_menu_selected(st7789v_input);
                    if(prim_menu_idx == MAXABS){
                        prim_menu_idx = 0;
                        y_per_v = (float)graph_pixel_Y_range/(float)user_max_v;
                        y_per_a = (float)graph_pixel_Y_range/(float)user_max_i;
                        y_per_p = (float)graph_pixel_Y_range/(float)user_max_w;
                        ESP_LOGW(ST7789V_TAG,"Pix/V:%f Pix/A:%f Pix/W:%f",y_per_v,y_per_a,y_per_p);
                    }
                }
            }else if((incoming_notification & 0x0F) == SELECTED_MENU){
                // xTaskNotify(ina219_task,ST7789V_NOTIFY_BUSY,eSetValueWithOverwrite);
                xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                if((incoming_notification & 0x0f0) == PRESSED) display_menus(st7789v_input);
                else display_menu_selected(st7789v_input);
            }else if((incoming_notification & 0x0F) == SELECTED_OPTION){
                xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                display_options(st7789v_input);
            }else if((incoming_notification & 0x0F) == EXIT_OPTION){
                xTaskNotify(hmi_task,ST7789V_NOTIFY_RECEIVED,eSetValueWithoutOverwrite);
                display_option_selected(st7789v_input);
                y_per_v = (float)graph_pixel_Y_range/(float)user_max_v;
                y_per_a = (float)graph_pixel_Y_range/(float)user_max_i;
                y_per_p = (float)graph_pixel_Y_range/(float)user_max_w;
                ESP_LOGW(ST7789V_TAG,"Pix/V:%f Pix/A:%f Pix/W:%f",y_per_v,y_per_a,y_per_p);
            }
            else if((incoming_notification & 0x0F) == EXIT_MENU) reset_menu(st7789v_input);

            //UNKNOWN NOTIFICATION ERROR
            else ESP_LOGE(ST7789V_TAG,"UNKNOWN notification recieved %x",incoming_notification);
        }//Check from where the notification came
    }
}
