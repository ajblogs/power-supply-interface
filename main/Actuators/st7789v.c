/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Actuators/st7789v.h"
static const char* ST7789V_TAG = "ST7789V";
#include "st7789v_psu_interface.h"


//Initialize the display
void lcd_init(spi_device_handle_t spi)
{
    int cmd=0;

    //Initialize non-SPI GPIOs
    gpio_set_direction(PIN_NUM_DC, GPIO_MODE_OUTPUT);
    gpio_set_direction(PIN_NUM_RST, GPIO_MODE_OUTPUT);
    gpio_set_direction(PIN_NUM_BCKL, GPIO_MODE_OUTPUT);

    //Reset the display
    gpio_set_level(PIN_NUM_RST, 0);
    vTaskDelay(100 / portTICK_RATE_MS);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(100 / portTICK_RATE_MS);

    //Send all the commands
    while (st_init_cmds[cmd].databytes!=0xff) {
        lcd_cmd(spi, st_init_cmds[cmd].cmd);
        lcd_data(spi, st_init_cmds[cmd].data, st_init_cmds[cmd].databytes&0x1F);
        if (st_init_cmds[cmd].databytes&0x80) {
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        cmd++;
    }

    ///Enable backlight
    gpio_set_level(PIN_NUM_BCKL, 1);
}

//This function is called (in irq context!) just before a transmission starts. It will
//set the D/C line to the value indicated in the user field.
void lcd_spi_pre_transfer_callback(spi_transaction_t *t)
{
    int dc=(int)t->user;
    gpio_set_level(PIN_NUM_DC, dc);
}

spi_device_handle_t spi_setup(){

    esp_err_t ret;
    spi_device_handle_t spi;
    spi_bus_config_t buscfg={
        .miso_io_num=-1,
        .mosi_io_num=PIN_NUM_MOSI,
        .sclk_io_num=PIN_NUM_CLK,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz=PARALLEL_LINES*PIX_WIDTH*2+8
    };

    spi_device_interface_config_t devcfg={
#ifdef CONFIG_LCD_OVERCLOCK

        .clock_speed_hz=30*1000*1000,           //Clock out at 26 MHz
#else
        .clock_speed_hz=10*1000*1000,           //Clock out at 10 MHz
#endif
        .mode=0,                                //SPI mode 0
        .spics_io_num=PIN_NUM_CS,               //CS pin
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
        .pre_cb=lcd_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
        .flags=SPI_DEVICE_NO_DUMMY,
    };
    //Initialize the SPI bus
    ret=spi_bus_initialize(VSPI_HOST, &buscfg, 1);
    ESP_ERROR_CHECK(ret);
    //Attach the LCD to the SPI bus
    ret=spi_bus_add_device(VSPI_HOST, &devcfg, &spi);
    ESP_ERROR_CHECK(ret);

    //Initialize the LCD
    // lcd_cmd(spi,0x1);
    lcd_init(spi);

    return spi;
}

/* Send a command to the LCD. Uses spi_device_polling_transmit, which waits
 * until the transfer is complete.
 *
 * Since command transactions are usually small, they are handled in polling
 * mode for higher speed. The overhead of interrupt transactions is more than
 * just waiting for the transaction to complete.
 */
void lcd_cmd(spi_device_handle_t spi, const uint8_t cmd)
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&cmd;               //The data is the cmd itself
    t.user=(void*)0;                //D/C needs to be set to 0
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}

/* Send data to the LCD. Uses spi_device_polling_transmit, which waits until the
 * transfer is complete.
 *
 * Since data transactions are usually small, they are handled in polling
 * mode for higher speed. The overhead of interrupt transactions is more than
 * just waiting for the transaction to complete.
 */
void lcd_data(spi_device_handle_t spi, const uint8_t *data, int len)
{
    assert(spi != NULL);
    esp_err_t ret;
    spi_transaction_t t;
    if (len==0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=len*8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer=data;               //Data
    t.user=(void*)1;                //D/C needs to be set to 1
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}

/* To send a set of lines we have to send a command, 2 data bytes, another command, 2 more data bytes and another command
 * before sending the line data itself; a total of 6 transactions. (We can't put all of this in just one transaction
 * because the D/C line needs to be toggled in the middle.)
 * This routine queues these commands up as interrupt transactions so they get
 * sent faster (compared to calling spi_device_transmit several times), and at
 * the mean while the lines for next transactions can get calculated.
 */
void send_lines(int ypos, spi_device_handle_t spi, uint16_t *linedata)
{
    assert(spi!=NULL);
    esp_err_t ret;
    int x;
    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans[6];

    //In theory, it's better to initialize trans and data only once and hang on to the initialized
    //variables. We allocate them on the stack, so we need to re-init them each call.
    for (x=0; x<6; x++) {
        memset(&trans[x], 0, sizeof(spi_transaction_t));
        if ((x&1)==0) {
            //Even transfers are commands
            trans[x].length=8;
            trans[x].user=(void*)0;
        } else {
            //Odd transfers are data
            trans[x].length=8*4;
            trans[x].user=(void*)1;
        }
        trans[x].flags=SPI_TRANS_USE_TXDATA;
    }
    ypos += PIX_HEIGHT_START;
    trans[0].tx_data[0]=0x2A;           //Column Address Set
    trans[1].tx_data[0]=PIX_WIDTH_START>>8;              //Start Col High
    trans[1].tx_data[1]=PIX_WIDTH_START&0xff;              //Start Col Low
    trans[1].tx_data[2]=(PIX_WIDTH_END)>>8;       //End Col High
    trans[1].tx_data[3]=(PIX_WIDTH_END)&0xff;     //End Col Low
    trans[2].tx_data[0]=0x2B;           //Page address set
    trans[3].tx_data[0]=ypos>>8;        //Start page high
    trans[3].tx_data[1]=ypos&0xff;      //start page low
    trans[3].tx_data[2]=(ypos+PARALLEL_LINES)>>8;    //end page high
    trans[3].tx_data[3]=(ypos+PARALLEL_LINES)&0xff;  //end page low
    trans[4].tx_data[0]=0x2C;           //memory write
    trans[5].tx_buffer=linedata;        //finally send the line data
    trans[5].length=PIX_WIDTH*2*8*PARALLEL_LINES;          //Data length, in bits (16bit/pix)
    trans[5].flags=0; //undo SPI_TRANS_USE_TXDATA flag

    //Queue all transactions.
    for (x=0; x<6; x++) {
        ret=spi_device_queue_trans(spi, &trans[x], 1000/portTICK_PERIOD_MS);
        //SPI ERROR MANAGE
        assert(ret==ESP_OK);
    }

    //When we are here, the SPI driver is busy (in the background) getting the transactions sent. That happens
    //mostly using DMA, so the CPU doesn't have much to do here. We're not going to wait for the transaction to
    //finish because we may as well spend the time calculating the next line. When that is done, we can call
    //send_line_finish, which will wait for the transfers to be done and check their status.
}

void send_rect(int xpos,int ypos, int xWidth,int yHeight, spi_device_handle_t spi, uint16_t *sqdata)
{
    assert(spi != NULL);
    assert(xWidth>0);
    assert(yHeight>0);

    esp_err_t ret;
    int x;
    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans[6];

    //In theory, it's better to initialize trans and data only once and hang on to the initialized
    //variables. We allocate them on the stack, so we need to re-init them each call.
    for (x=0; x<6; x++) {
        memset(&trans[x], 0, sizeof(spi_transaction_t));
        if ((x&1)==0) {
            //Even transfers are commands
            trans[x].length=8;
            trans[x].user=(void*)0;
        } else {
            //Odd transfers are data
            trans[x].length=8*4;
            trans[x].user=(void*)1;
        }
        trans[x].flags=SPI_TRANS_USE_TXDATA;
    }
    ypos += PIX_HEIGHT_START;
    xpos += PIX_WIDTH_START;
    int xpose = xpos+xWidth-1; 
    int ypose = ypos+yHeight-1; 
    trans[0].tx_data[0]=0x2A;           //Column Address Set
    trans[1].tx_data[0]=xpos>>8;              //Start Col High
    trans[1].tx_data[1]=xpos&0xff;              //Start Col Low
    trans[1].tx_data[2]=xpose>>8;       //End Col High
    trans[1].tx_data[3]=xpose&0xff;     //End Col Low
    trans[2].tx_data[0]=0x2B;           //Page address set
    trans[3].tx_data[0]=ypos>>8;        //Start page high
    trans[3].tx_data[1]=ypos&0xff;      //start page low
    trans[3].tx_data[2]=ypose>>8;    //end page high
    trans[3].tx_data[3]=ypose&0xff;  //end page low
    trans[4].tx_data[0]=0x2C;           //memory write
    trans[5].tx_buffer=sqdata;        //finally send the line data
    trans[5].length=xWidth*2*8*yHeight;          //Data length, in bits (16bit/pix)
    trans[5].flags=0; //undo SPI_TRANS_USE_TXDATA flag

    //Queue all transactions.
    for (x=0; x<6; x++) {
        ret=spi_device_queue_trans(spi, &trans[x], 1000/portTICK_PERIOD_MS);
        //SPI ERROR MANAGE
        assert(ret==ESP_OK);
    }

    //When we are here, the SPI driver is busy (in the background) getting the transactions sent. That happens
    //mostly using DMA, so the CPU doesn't have much to do here. We're not going to wait for the transaction to
    //finish because we may as well spend the time calculating the next line. When that is done, we can call
    //send_line_finish, which will wait for the transfers to be done and check their status.
}

void send_finish(spi_device_handle_t spi)
{
    assert(spi != NULL);
    spi_transaction_t *rtrans;
    esp_err_t ret;
    //Wait for all 6 transactions to be done and get back the results.
    for (int x=0; x<6; x++) {
        ret=spi_device_get_trans_result(spi, &rtrans, 1000/portTICK_PERIOD_MS);
        //SPI ERROR MANAGE
        assert(ret==ESP_OK);
        //We could inspect rtrans now if we received any info back. The LCD is treated as write-only, though.
    }
}

/* Used to convert 24bit color depth to 16bit color depth by dropping the LSBs. The data 
 * is rearranged in 16bits as g(2:0)b(4:0)r(4:0)g(5:3)
*/
color rgb24_16(color rgb){

    color r = rgb & 0xf80000;
    // r = r>>19; printf("Red value at %x\n",r);
    // r = r<<3;
    r = r>>16;

    color g = rgb & 0x00fc00;
    color g_h = g >> 13;
    g = g_h | ((g & 0x001c00)<<3); 

    color b = rgb & 0x0000f8;
    // b = b>>3; printf("Blue value at %x\n",b);
    // b = b<<8;
    b = b<<5;
    
    return (0xffff^(r|g|b));
}
 //GENERAL
void lcd_fill(color rgb24, lcd_struct *lcd){
    //Create the container for holding all the data 
    uint16_t *dd = heap_caps_malloc(PARALLEL_LINES*PIX_WIDTH*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);
    color rgb16 = rgb24_16(rgb24);
    for(int i=0;i<PARALLEL_LINES*PIX_WIDTH;i++){
        *(dd+i) = rgb16;
    }
    for(int i=0;i<PIX_HEIGHT;i+=PARALLEL_LINES){
        send_lines(i,lcd->spi,dd);
        send_finish(lcd->spi);
    }
    heap_caps_free(dd);
}

void lcd_rect(color rgb24, int xpos,int ypos, int xWidth,int yHeight, lcd_struct *lcd){
    //Create the container for holding all the data 
    uint16_t *dd = heap_caps_malloc(xWidth*yHeight*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);
    color rgb16 = rgb24_16(rgb24);
    for(int i=0;i<(xWidth*yHeight);i++){
        *(dd+i) = rgb16;
    }    
    send_rect(xpos,ypos,xWidth,yHeight,lcd->spi,dd);
    send_finish(lcd->spi);
    heap_caps_free(dd);
}

//TEXT SECTION
/*Decode the print data from raw coded fonts */
void font16_decoder(font_struct *f, char c, uint16_t* disp_data){
    
    int height = f->height;
    int char_idx = c-f->firstchr_idx;
    int font_wdth = f->widtbl[char_idx];
    const unsigned char *chr_code = f->chrtbl[char_idx];
    color font_rgb16 = rgb24_16(f->font_rgb24);
    color backgnd_rgb16;
    if(f->font_back_rgb24 == -1) backgnd_rgb16 = rgb24_16(f->lcd->background); 
    else backgnd_rgb16 = rgb24_16(f->font_back_rgb24);

    // Bytes per pixel coded in the Font16.h
    int bpp_coded = 0;
    if(font_wdth > 8) bpp_coded = 2;
    else bpp_coded = 1;
    assert(bpp_coded > 0 && bpp_coded < 3);

    for(int i=0;i<(height*bpp_coded);i+=bpp_coded){
        uint16_t code=chr_code[i]<<8;
        if(font_wdth>8) code |= chr_code[i+1];
        // printf("Lets see the code %x\n",code);
        for(int j=0;j<font_wdth;j++){
            if((code<<j) & 0x8000) *(disp_data+((i/bpp_coded)*font_wdth)+j) = font_rgb16;
            else *(disp_data+((i/bpp_coded)*font_wdth)+j) = backgnd_rgb16;
        }
    } 
}

/*Decode the print data from running length encoded (RLE) fonts */
void font_rle_decoder(font_struct *f, char c, uint16_t* disp_data){
    
    int char_hght = f->height;
    int char_idx = c-f->firstchr_idx;
    int char_wdth = f->widtbl[char_idx];
    const unsigned char *chr_code = f->chrtbl[char_idx];
    color font_rgb16 = rgb24_16(f->font_rgb24);
    color backgnd_rgb16;
    if(f->font_back_rgb24 == -1) backgnd_rgb16 = rgb24_16(f->lcd->background); 
    else backgnd_rgb16 = rgb24_16(f->font_back_rgb24);

    //Total number of pixels per character.
    int char_pixs = char_wdth*char_hght;

    int pix_cnt_all = 0;
    int idx = 0;
    while(1){
        int code = chr_code[idx];
        int pix_data = code & 0x80;
        int pix_cnt = (code & 0x7F)+1;
        for(int i = pix_cnt_all;i<(pix_cnt+pix_cnt_all);i++){
            if(pix_data) *(disp_data+i) = font_rgb16;
            else *(disp_data+i) = backgnd_rgb16;
        }
        pix_cnt_all += pix_cnt; 
        if(pix_cnt_all == char_pixs) return;
        idx++;
    }
}

/*Print a character on LCD*/
void lcd_char(int xpos,int ypos, char c, font_struct *font){

    uint16_t *dd1 = heap_caps_malloc(font->height*font->max_width*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd1 != NULL);
    int char_idx = c-(font->firstchr_idx);
    int char_wdth = font->widtbl[char_idx];

    int char_hght = font->height;
    font->decoder(font,c,dd1);
   
    send_rect(xpos,ypos,char_wdth,char_hght,font->lcd->spi,dd1);
    send_finish(font->lcd->spi);
    heap_caps_free(dd1);
}

/*Print the string on LCD*/
int lcd_text(int xpos,int ypos, char *c, font_struct *font){
    
    int firstchr_idx = font->firstchr_idx;
    const unsigned char  *widtbl = font->widtbl;
    uint16_t *dd1 = heap_caps_malloc(font->height*font->max_width*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd1 != NULL);
    uint16_t *dd2 = heap_caps_malloc(font->height*font->max_width*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd2 != NULL);

    int idx=0;
    while (1)
    {   
        int id = c[idx-1]-(firstchr_idx);

        if(c[idx] == '\0'){
            send_finish(font->lcd->spi);
            heap_caps_free(dd1);
            heap_caps_free(dd2);
            return xpos+widtbl[id];
        }
        else{
            if(idx) xpos = xpos+widtbl[id];
            int char_idx = c[idx]-(font->firstchr_idx);
            int char_wdth = font->widtbl[char_idx];
            int char_hght = font->height;

            //Decoding the the pixel data while waiting for the previous spi sends to finish
            //odd
            if(idx%2==1){                   
                font->decoder(font,c[idx],dd1);
                send_finish(font->lcd->spi);
                send_rect(xpos,ypos,char_wdth,char_hght,font->lcd->spi,dd1);
            }
            //even
            else{
                font->decoder(font,c[idx],dd2);
                if(idx != 0) send_finish(font->lcd->spi);
                send_rect(xpos,ypos,char_wdth,char_hght,font->lcd->spi,dd2);
            }
        }
        idx++;
    }
}

/*Algorithm to plot two values with their respective color. Values are chosen based on the bool val*/
//GRAPH SECTION
static int V_x = 0;
static int A_x = 0;
void graph_plot(int val, bool VOLT, graph_struct *graph_settings){

    int sheet_height = graph_settings->sheet_height;
    int graph_width = graph_settings->plot_width;
    int sheet_width = graph_settings->sheet_width;
    color graph_rgb16;
    color graph_back_rgb16;
    if(Volt){
        graph_rgb16 = rgb24_16(graph_settings->plot_V_color);
        graph_back_rgb16 = rgb24_16(graph_settings->back_V_color);
    }
    else{
        graph_rgb16 = rgb24_16(graph_settings->plot_A_color);
        graph_back_rgb16 = rgb24_16(graph_settings->back_A_color);  
    }
    
    // lcd_rect(0xFFFFFF,graph_settings->A_x_offset,,50,100,&lcd);

    uint16_t *dd = heap_caps_malloc(sheet_height*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);

    // +(Y_ranage/2) to -(sheet_height/2) is the acceptable val
    assert((-sheet_height/2)<=val && val<=(sheet_height/2));
    
    for(int i=0;i<sheet_height;i++){
        if(val>0 && i<=(sheet_height/2)){    
            if(i >= ((sheet_height/2)-val)) *(dd+i) = graph_rgb16;
            else *(dd+i) = graph_back_rgb16;          
        }
        else if(val<0 && i>=(sheet_height/2)){
            if(i<((sheet_height/2)-val)) *(dd+i) = graph_rgb16;
            else *(dd+i) = graph_back_rgb16;
        }
        else *(dd+i) = graph_back_rgb16;
    }
    if(VOLT){
        send_rect((V_x+graph_settings->V_x_offset),graph_settings->V_y_offset,graph_width,sheet_height,graph_settings->lcd->spi,dd);
        V_x++;
        if(V_x>(sheet_width-1)) V_x = 0;
    }else{
        send_rect((A_x+graph_settings->A_x_offset),graph_settings->A_y_offset,graph_width,sheet_height,graph_settings->lcd->spi,dd);        
        A_x++;
        if(A_x>(sheet_width-1)) A_x = 0;
    }
    send_finish(graph_settings->lcd->spi);
    heap_caps_free(dd);
}

/*Algorithm to plot <data_length> values/x with their respective color*/
// static int Mx = 0;
int graph_merge_plot(int input_data[data_length][2], merge_graph_struct *graph_settings){
    bool positive_graph = graph_settings->only_positive;
    int sheet_height = graph_settings->sheet_height; //be Even
    int sheet_width = graph_settings->sheet_width;
    int graph_width = graph_settings->plot_width;
    int merge_x_offset = graph_settings->merge_x_offset;
    color yaxis_rgb16 = rgb24_16(graph_settings->Y_axis_color);
    color graph_back_rgb16 = rgb24_16(graph_settings->merge_back_color);
    color cursor_color_rgb16 =  rgb24_16(graph_settings->cursor_color);
    // input_data[value][color]
    for(int i=0;i<data_length;i++){
        if(positive_graph) assert(0<=input_data[i][qty] && input_data[i][qty]<=(sheet_height-1));
        else assert((-sheet_height/2)<=input_data[i][qty] && input_data[i][qty]<=(sheet_height/2));
    }
    int Y_axis_pos ;
    if(positive_graph) Y_axis_pos = (sheet_height-1);
    else Y_axis_pos = (sheet_height-1)/2;
    int Y_axis_neg = (sheet_height+1)/2;
    uint16_t *dd = heap_caps_malloc(sheet_height*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);

    for (int recursive = 0; recursive<data_length; recursive++){
        for (int idx = 0; idx<data_length; idx++){
        //Flip the greater/lesser symbol to change the decreasing/increasing order. Don't change it! as this assumption is used in the following
            int next_idx = idx+1;
            int i = idx;
            if(!(next_idx<data_length)){
                next_idx = data_length-1;
                i = 0;
            }
            if(input_data[i][qty]>input_data[next_idx][qty]){
                //swap quantity
                int temp = input_data[i][qty];
                input_data[i][qty] = input_data[next_idx][qty];
                input_data[next_idx][qty] = temp;

                //swap attribute (color)
                temp = input_data[i][attr];
                input_data[i][attr] = input_data[next_idx][attr];
                input_data[next_idx][attr] = temp;
            } 
        }
    }
    // for (int a = 0;a<data_length;a++){
    //     ESP_LOGW(ST7789V_TAG,"%x is the val and %x is the color",input_data[a][qty],input_data[a][attr]);
    // }

    //assumed lower index has minimum value. Moving from higher to lower values
    int pix_y = 0;
    int next_idx = 0;
    for(int idx = data_length-1; idx >= 0; idx--){
            
        if(idx != 0) next_idx = idx-1; //Not the last quantity
        else next_idx = idx;

        if(idx == data_length-1){//First val and the maximum value
            //Fill the values higher than this with back color
            pix_y = 0;
            if(input_data[idx][qty]>0){
                for (;pix_y<(Y_axis_pos-input_data[idx][qty]);pix_y++){
                    *(dd+pix_y) = graph_back_rgb16;
                    ESP_LOGV(ST7789V_TAG,"%d is the pix_y, Begining + for sort %d",pix_y,input_data[idx][qty]);
                    //assert(pix_y>=0 && pix_y<sheet_height);
                }
                //assert(pix_y==(Y_axis_pos-input_data[idx][qty]));
            }
            else { //Maximum value is a negative value. Fill the +ve axis with back color
                for (;pix_y<Y_axis_pos;pix_y++){
                    *(dd+pix_y) = graph_back_rgb16;
                    ESP_LOGV(ST7789V_TAG,"%d is the pix_y, Begining - for sort %d",pix_y,input_data[idx][qty]);
                    //assert(pix_y>=0 && pix_y<sheet_height);
                }
                //assert(pix_y == Y_axis_pos);
                *(dd+pix_y) = yaxis_rgb16;pix_y++;    
            }
        }

        if(idx != 0 && input_data[idx][qty] == input_data[next_idx][qty]){
            //Mixing colors
            input_data[next_idx][attr] += input_data[idx][attr];
            input_data[next_idx][attr] /= 2;
        }
        else if (idx != 0){
            if(input_data[idx][qty] > 0){
                if(input_data[next_idx][qty] > 0){
                    for(;pix_y<(Y_axis_pos-input_data[next_idx][qty]);pix_y++){
                        *(dd+pix_y) = input_data[idx][attr];
                        ESP_LOGV(ST7789V_TAG,"%d is the pix_y, sort qty = %d",pix_y,input_data[idx][qty]);
                        //assert(pix_y>=0 && pix_y<sheet_height);
                    }
                }else{
                    for(;pix_y<Y_axis_pos;pix_y++){
                        *(dd+pix_y) = input_data[idx][attr];
                        ESP_LOGV(ST7789V_TAG,"%d is the pix_y, sort qty = %d",pix_y,input_data[idx][qty]);
                        //assert(pix_y>=0 && pix_y<sheet_height);
                    }
                    *(dd+pix_y) = yaxis_rgb16;pix_y++;    
                }
            }else if(input_data[idx][qty] < 0){
                for(;pix_y<(Y_axis_neg+(input_data[idx][qty]*-1));pix_y++){
                    *(dd+pix_y) = input_data[idx][attr];
                    ESP_LOGV(ST7789V_TAG,"%d is the pix_y, sort qty = %d",pix_y,input_data[idx][qty]);
                    //assert(pix_y>=0 && pix_y<sheet_height);
                }                    
            }
        }   
        else{ // last value
                if(input_data[idx][qty] > 0){
                    for(;pix_y<Y_axis_pos;pix_y++){
                        *(dd+pix_y) = input_data[idx][attr];
                        ESP_LOGV(ST7789V_TAG,"%d is the pix_y, sort qty = %d, Last value +",pix_y,input_data[idx][qty]);
                        // *(dd+pix_y) = yaxis_rgb16;pix_y++;    
                        //assert(pix_y>=0 && pix_y<sheet_height);
                    }
                    *(dd+pix_y) = yaxis_rgb16;pix_y++;    
                }else if(input_data[idx][qty] < 0){
                    for(;pix_y<(Y_axis_neg+(input_data[idx][qty]*-1));pix_y++){
                        *(dd+pix_y) = input_data[idx][attr];
                        ESP_LOGV(ST7789V_TAG,"%d is the pix_y, sort qty = %d, Last value -",pix_y,input_data[idx][qty]);
                        //assert(pix_y>=0 && pix_y<sheet_height);
                    }                    
                }
            }
        
        //After last value, fill with back color 
        if (next_idx == idx){
            while(pix_y<sheet_height){
                *(dd+pix_y) = graph_back_rgb16;
                pix_y++;
            }
        }
    }
    send_rect((Mx+merge_x_offset),graph_settings->merge_y_offset,graph_width,sheet_height,graph_settings->lcd->spi,dd);
    send_finish(graph_settings->lcd->spi);
    heap_caps_free(dd);
    Mx++;
    if(Mx>(sheet_width-1)) Mx = 0;

    //Cursor update    
    dd = heap_caps_malloc(sheet_height*sizeof(uint16_t),MALLOC_CAP_DMA);
    assert(dd != NULL);
    for(int i=0;i<sheet_height;i++){
        *(dd+i) = cursor_color_rgb16;
    }
    send_rect((Mx+merge_x_offset),graph_settings->merge_y_offset,graph_width,sheet_height,graph_settings->lcd->spi,dd);
    send_finish(graph_settings->lcd->spi);
    heap_caps_free(dd);
    return Mx+merge_x_offset;
}
