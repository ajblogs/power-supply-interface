/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Actuators/virtual_control.h"
static const char* CONTROL = "CTRL";
static uint8_t ctrl_pin = 0x00;

void setup_ctrl_pin(uint8_t pin){
    gpio_pad_select_gpio(pin);
    ESP_ERROR_CHECK(gpio_set_direction(pin,GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(pin,0));
    ctrl_pin = pin;
}

void power_up(){
    ESP_ERROR_CHECK(gpio_set_level(ctrl_pin,1));
}

void power_down(){
    ESP_ERROR_CHECK(gpio_set_level(ctrl_pin,0));
}

void power_control_routine(void *args){
    vTaskDelay(100/portTICK_PERIOD_MS);
    ctrl_struct *ctrl_input = (ctrl_struct *)args;
    const xTaskHandle hmi_task = *(ctrl_input->hmi_task);
    const xTaskHandle ina219_task = *(ctrl_input->ina219_task);
    uint32_t incoming_notification = 0;
    bool V_lock = false;
    bool A_lock = false;
    bool W_lock = false;
    bool INA_lock = false;
    bool power = false;
    while(1){
        // vTaskDelay(100/portTICK_PERIOD_MS);
        ESP_LOGW(CONTROL,"Ctrl check! %x",incoming_notification);
        if(xTaskNotifyWait(0x0,0x0,&incoming_notification,portMAX_DELAY)){
            if(incoming_notification == INA219_OVERCURRENT_ERROR){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                A_lock = true;
                power = false;
                power_down();
            }else if (incoming_notification == INA219_OVERVOLTAGE_ERROR){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                V_lock = true;
                power = false;
                power_down();
            }else if (incoming_notification == INA219_OVERWATT_ERROR){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                W_lock = true;
                power = false;
                power_down();
            }
            else if (incoming_notification == INA219_DISCONNECTED){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                INA_lock = true;
                power = false;
                power_down();
            }
            else if(incoming_notification == INA219_OVERCURRENT_ERROR_HANDLED){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                A_lock = false;
            }
            else if(incoming_notification == INA219_OVERVOLTAGE_ERROR_HANDLED){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                V_lock = false;
            }
            else if(incoming_notification == INA219_OVERWATT_ERROR_HANDLED){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                W_lock = false;
            }
            else if(incoming_notification == INA219_RECONNECTED){
                xTaskNotify(ina219_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                INA_lock = false;
            }
            else if((incoming_notification & 0x0f) == TOGGLING_PSU_OUT){
                if(!A_lock && !V_lock && !W_lock && !INA_lock){
                    xTaskNotify(hmi_task,CTRL_ACKNOWLEDGED,eSetValueWithOverwrite);
                    power = !power;
                    if(power) power_up();
                    else power_down();
                }else{
                        xTaskNotify(hmi_task,CTRL_NACKNOWLEDGED,eSetValueWithOverwrite);
                        ESP_LOGE(CONTROL,"Can't turn on due to ongoing lockdown. Check the screen for reason");
                    }   
            }else ESP_LOGE(CONTROL,"Irrelevent notification at this time %x",incoming_notification);
        }
        else ESP_LOGE(CONTROL,"This should never happen!");
    }
}   
