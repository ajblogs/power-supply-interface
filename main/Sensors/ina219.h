/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/i2c.h"

#define I2C_MASTER_TX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

/*INA219 Register address map*/
#define INA219_SENSOR_ADDR  0x40                /*!<INA219 sensor's address */
#define INA219_CONFIG_REG_ADDR 0x00             
#define INA219_SHUNT_VOLTAGE_REG_ADDR 0x01
#define INA219_BUS_VOLTAGE_REG_ADDR 0x02
#define INA219_POWER_REG_ADDR 0x03
#define INA219_CURRENT_REG_ADDR 0x04
#define INA219_CALIB_REG_ADDR 0x05

//Calibration calculation procedure according to INA219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf, Section 8.5.1
#define INA219_SHUNT_RESISTOR_VAL 0.1           /*!<INA129 sensor's shunt resistor value in ohms*/
#define INA219_CALIBRATION_VAL_FOR_1_AMP_MAX_1_OHM_SHUNT 1342.1773
#define INA219_CALIBRATION_VAL_FOR_1_AMP_MAX INA219_CALIBRATION_VAL_FOR_1_AMP_MAX_1_OHM_SHUNT/INA219_SHUNT_RESISTOR_VAL
#define INA219_SHUNT_VOLTAGE_LSB 0.00001
#define INA219_CURRENT_MULTIPLIER 3.051757
#define INA219_CURRENT_DIVIDER 100000.0
#define INA219_POWER_MULTIPLIER INA219_CURRENT_MULTIPLIER*2.0
#define INA219_POWER_DIVIDER 10000.0

typedef struct {
   uint16_t raw_current;
   uint16_t raw_voltage;
   uint16_t raw_power;
   uint16_t raw_shunt_voltage;
   float Pcalc;
   float Icalc;
   float P;
   float I;
   float V;
   bool stale;
   bool overflow;
   bool cnv_time_err;
   bool disconnected;
   xTaskHandle * display_task;
   xTaskHandle * ctrl_task;
} ina219_struct;

i2c_port_t i2c_master_init();

void configure_INA219(uint16_t config_data);

void calicon_INA219(float max_ampere,uint16_t config_data);

void reset_INA219();

esp_err_t write_INA219(uint8_t reg_addr, uint16_t *data);

uint16_t read_INA219(uint8_t reg_addr);

void getdata_INA219(ina219_struct *);

void ina219_routine(void *ina219_data_pointer);