/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "system_definitions.h"
#include "driver/gpio.h"

typedef enum {
    BLANK= 1,
    DISPLAY_PRIMARY_MENU,
    SELECTED_MENU,
    SELECTED_OPTION,
    EXIT_OPTION,
    EXIT_MENU,
} interface_status;

typedef enum {
    PRESSED = 0x10,
    RELEASED =0x20 ,
} button_status;

typedef struct {
    interface_status status;
    button_status button;
    uint8_t primary_menu;
    uint8_t menu;
    uint8_t option;
    xTaskHandle * display_task;
    xTaskHandle * ctrl_task;
} hmi_struct;

void setup_user_button(uint8_t);

bool user_button_pressed();

void hmi_check_n_update(hmi_struct*);

void hmi_routine(void *hmi_data_pointer);

