/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Sensors/ina219.h"
static const char* INA219_TAG = "INA219";
#include "Sensors/ina219_psu_interface.h"

i2c_port_t i2c_master_init()
{
    i2c_port_t i2c_master_port = I2C_MASTER_INA219_PORT;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode,
                              I2C_MASTER_RX_BUF_DISABLE,
                              I2C_MASTER_TX_BUF_DISABLE, 0));
    return i2c_master_port;
}

esp_err_t write_INA219(uint8_t reg_addr, uint16_t *data)
{
    //Address must point to one of the INA219's accessible register
    assert(reg_addr<0x06);
    uint8_t data_h = (*data>>8);
    uint8_t data_l = (*data & 0xFF);
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();    
    
    //The write procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf,  Figure 15
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA219_SENSOR_ADDR<<1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_h, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_l, ACK_CHECK_EN);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_INA219_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK) {
        //IIC ERROR HANDLE
    }
    return ret;
}

uint16_t read_INA219(uint8_t reg_addr)
{
    //Address must point to one of the INA219's accessible register
    assert(reg_addr<0x06);
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    uint8_t data_l=0xff;
    uint8_t data_h=0xff;

    //Address pointer set procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf, Figure 18
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA219_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_INA219_PORT, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK) {
        //IIC ERROR HANDLE
        ESP_LOGD(INA219_TAG,"Error writing register");
    }

    //The read procedure according to IN219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf, Figure 16
    // vTaskDelay(1/portTICK_PERIOD_MS);
    cmd = i2c_cmd_link_create();   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA219_SENSOR_ADDR << 1) | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, &data_h, ACK_VAL);
    i2c_master_read_byte(cmd, &data_l, NACK_VAL);
    i2c_master_stop(cmd);

    ret = i2c_master_cmd_begin(I2C_MASTER_INA219_PORT, cmd,  1000 / portTICK_RATE_MS);
    if (ret != ESP_OK) {
        //IIC ERROR HANDLE
        ESP_LOGD(INA219_TAG,"Error reading register");
    }   
    i2c_cmd_link_delete(cmd);
    return ((data_h << 8) | data_l);
}

void reset_INA219(){
    uint16_t reset = 0x8000;
    ESP_ERROR_CHECK(write_INA219(INA219_CONFIG_REG_ADDR,&reset));
}

//Configure the sensor settings. Improper PGA settings might defect the shunt voltage amplifier
void configure_override_INA219(uint16_t config_data)
{
    reset_INA219();
    vTaskDelay(1/portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(write_INA219(INA219_CONFIG_REG_ADDR,&config_data));
}

//Configure the sensor settings. Improper PGA settings might defect the shunt voltage amplifier
void calibration_override_INA219(uint16_t cal_constant)
{
    ESP_ERROR_CHECK(write_INA219(INA219_CALIB_REG_ADDR,&cal_constant));
}

static float INA219_CURRENT_LSBx100000 = 0;
static float INA219_POWER_LSBx10000 = 0;
//Calibrate the sensor and set the PGA based on max current
void calicon_INA219(float max_ampere,uint16_t config_data)
{
    // Check and set max ampere expected to be sensed
    assert(max_ampere>0.0);
    if(max_ampere > INA219_MAX_SENSE_CURRENT){
        ESP_LOGE(INA219_TAG,"Max allowed sense current is %dA. Unable to set the requested max_current value !",INA219_MAX_SENSE_CURRENT);
        while(1);
    }
    
    // Claculate LSB values of POWER and CURRENT
    INA219_MAX_AMPERE = max_ampere;
    INA219_CURRENT_LSBx100000 = max_ampere*(float)INA219_CURRENT_MULTIPLIER;
    INA219_POWER_LSBx10000 = max_ampere*(float)INA219_POWER_MULTIPLIER;

    // Cross check the shunt voltage equivalen for given max_current and set PGA accordingly
    uint16_t pga = 0x3;
    float cross_check_ma = max_ampere * INA219_SHUNT_RESISTOR_VAL;
    if(cross_check_ma<0.040) pga = 0;
    else if(cross_check_ma<0.080) pga = 1;
    else if(cross_check_ma<0.160) pga = 2;
    else if(cross_check_ma<0.320) pga = 3;
    else{
        ESP_LOGE(INA219_TAG,"Unable to configure PGA. This should never happen!");     
        exit(EXIT_FAILURE);
    }
    config_data = (config_data & 0xE7FF) | (pga<<11);
    ESP_LOGI(INA219_TAG,"Configuration register set: %#04x", config_data);
    configure_override_INA219(config_data);

    float calc = ((float)INA219_CALIBRATION_VAL_FOR_1_AMP_MAX/max_ampere);
    //Calibration constant rounded off to nearest 1
    uint16_t cal_constant = ((calc+0.5)*100.0)/100.0;
    cal_constant &= 0xfffe;
    ESP_LOGW(INA219_TAG,"Calibration value: %f Calibration register set: %#04x",calc,cal_constant);
    ESP_ERROR_CHECK(write_INA219(INA219_CALIB_REG_ADDR,&cal_constant));
    ESP_LOGI(INA219_TAG, "Voltage_LSB: %f Current_LSB*100000: %f Power_LSB*10000: %f",INA219_VOLTAGE_LSB,INA219_CURRENT_LSBx100000,INA219_POWER_LSBx10000);

    //Cross check written value
    if((read_INA219(INA219_CALIB_REG_ADDR) == cal_constant) && (read_INA219(INA219_CONFIG_REG_ADDR) == config_data)){
        ESP_LOGI(INA219_TAG,"Readback check calibarion and configuration value: OK");
    }else{
        ESP_LOGE(INA219_TAG,"Readback check calibarion and configuration value: FAIL");
        exit(1);
    }
}

void getdata_INA219(ina219_struct *idat){
    uint16_t raw_volt=0xffff,raw_current=0xffff,raw_power=0xffff,raw_shunt_volt=0xffff; 
    raw_volt = read_INA219(INA219_BUS_VOLTAGE_REG_ADDR);
    // Conversion Ready check
    if(raw_volt == 0xFFFF){     
        idat->overflow = false;
        idat->cnv_time_err = false;
        idat->stale = true;
        if(!INA219_connected()){
            idat->disconnected = true;
            return;
        }
    }
    idat->disconnected = false;
    if(raw_volt & 0b10){     
        //Overflow check
        if(raw_volt & 0b01) ESP_LOGE(INA219_TAG,"Overflow error: Short circuit detected!");
        //     idat->overflow = true;
        //     idat->cnv_time_err = false;
        //     idat->stale = true;
        //     return;
        // }else{
            raw_shunt_volt = read_INA219(INA219_SHUNT_VOLTAGE_REG_ADDR);
            raw_current = read_INA219(INA219_CURRENT_REG_ADDR);
            raw_power = read_INA219(INA219_POWER_REG_ADDR);
            float I = ((float)((int16_t)raw_current)*(float)INA219_CURRENT_LSBx100000)/INA219_CURRENT_DIVIDER;
            float V = (float)(raw_volt>>3)*(float)INA219_VOLTAGE_LSB;
            float P = ((float)(raw_power)*(float)INA219_POWER_LSBx10000)/INA219_POWER_DIVIDER;

            idat->stale = false;
            idat->overflow = false;
            idat->cnv_time_err = false;
            idat->raw_current = raw_current;
            idat->raw_voltage = raw_volt;
            idat->raw_power = raw_power;
            idat->raw_shunt_voltage = raw_shunt_volt;
            idat->P = P;
            idat->I = I;
            idat->V = V;

            float Icalc = ((float)((int16_t)raw_shunt_volt)*(float)INA219_SHUNT_VOLTAGE_LSB)/(float)INA219_SHUNT_RESISTOR_VAL;
            idat->Icalc = Icalc;
            if(Icalc < 0) Icalc =- Icalc;  
            idat->Pcalc = (Icalc)*V;
            // printf("shunt:%#04x volt:%#04x curent:%#04x power:%#04x\n",raw_shunt_volt,raw_volt,raw_current,raw_power);
            // printf("%f is the voltage while %f is the current, %f is the power\n",V,Icalc,idat->Pcalc);
            return;
        // }
    }else{
        ESP_LOGE(INA219_TAG,"Insufficient time for conversion");
        idat->cnv_time_err = true;
        idat->overflow = false;
        idat->stale = true;
        return;
    }
}
