/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Sensors/hmi.h"
static const char* HMI = "HMI";
#include "Sensors/hmi_psu_interface.h"

static uint8_t user_button_pin = 0xFF;

void setup_user_button(uint8_t pin){
    gpio_pad_select_gpio(pin);
    ESP_ERROR_CHECK(gpio_set_direction(pin,GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_pull_mode(pin,GPIO_PULLUP_ONLY));
    user_button_pin = pin;
}

bool user_button_pressed(){
    assert(user_button_pin != 0xFF);
    return gpio_get_level(user_button_pin) == 0;
}
void notify_button_status(hmi_struct *hmi){
    uint32_t notification = 0;
    notification |= hmi->status;
    notification |= hmi->button;
    notification |= hmi->primary_menu<<8;
    notification |= hmi->menu<<16;
    notification |= hmi->option<<24;

    bool sent = false;
    while(!sent) {
        uint32_t notif_temp = 0;
        if(xTaskNotify(*hmi->display_task, notification,eSetValueWithoutOverwrite)){
            vTaskDelay(100/portTICK_PERIOD_MS);
            if(xTaskNotifyWait(0x0,0x0,&notif_temp, 0)){
                if(notif_temp == ST7789V_NOTIFY_RECEIVED) sent = true; 
                if(notif_temp != ST7789V_NOTIFY_RECEIVED) ESP_LOGE(HMI,"Irrelevent response %x",notification);
            } else  ESP_LOGE(HMI,"No response from ST7789V!");
        }
    }
}

void hmi_check_n_update(hmi_struct *hmi_input){
    if(user_button_pressed() && hmi_input->button != PRESSED){
        hmi_input->button = PRESSED;
        if(hmi_input->status == SELECTED_MENU){
            vTaskDelay(1000/portTICK_PERIOD_MS);
            if(user_button_pressed()){
                hmi_input->status = SELECTED_OPTION;
                while(user_button_pressed()){
                    hmi_input->option++;
                    ESP_LOGW(HMI,"OPTION %d",hmi_input->option);
                    notify_button_status(hmi_input);
                    vTaskDelay(options_display_timeperiod);
                    if(!user_button_pressed()) break;
                }
            }else{
                hmi_input->status = EXIT_OPTION; 
                ESP_LOGW(HMI,"EXIT OPTION");
                notify_button_status(hmi_input);
            }
        }else if(hmi_input->status == DISPLAY_PRIMARY_MENU){
            vTaskDelay(1000/portTICK_PERIOD_MS);
            if(user_button_pressed()){
                hmi_input->status = SELECTED_MENU;
                while(user_button_pressed()){
                    hmi_input->menu++;
                    ESP_LOGW(HMI,"SELECT_MENU %d", hmi_input->menu);
                    notify_button_status(hmi_input); 
                    vTaskDelay(menu_display_timeperiod);
                    if(!user_button_pressed()) break;
                }
            }else{
                hmi_input->status = EXIT_MENU;
                ESP_LOGW(HMI,"EXIT_MENU");
                notify_button_status(hmi_input);
            }
        }else{
            ESP_LOGW(HMI,"BUTTON PRESSED");
            vTaskDelay(1000/portTICK_PERIOD_MS);
            if(user_button_pressed()){
                hmi_input->status = DISPLAY_PRIMARY_MENU;
                while(user_button_pressed()){       
                    hmi_input->primary_menu++;
                    ESP_LOGW(HMI,"PRIMARY MENU %d",hmi_input->primary_menu);
                    notify_button_status(hmi_input);
                    vTaskDelay(primary_menu_display_timeperiod);
                    if(!user_button_pressed()) break;
                }
            }else{
                ESP_LOGW(HMI,"TOGGLE POWERSUPPLY OUTPUT");
                hmi_input->status = TOGGLING_PSU_OUT;
                uint32_t notification = 0;
                bool sent = false;
                while(!sent) {
                    if(xTaskNotify(*hmi_input->ctrl_task,TOGGLING_PSU_OUT,eSetValueWithoutOverwrite)){
                        vTaskDelay(100/portTICK_PERIOD_MS);
                        if(xTaskNotifyWait(0x0,0x0,&notification, 0)){
                            if(notification == CTRL_ACKNOWLEDGED){ 
                                sent =true;
                                notify_button_status(hmi_input);
                            }
                            else if(notification != CTRL_NACKNOWLEDGED) ESP_LOGE(HMI,"No response from ctrl!");
                            else sent = true;
                        }
                    }
                }
                
                // xTaskNotify(hmi_input->ctrl_task,TOGGLING_PSU_OUT,eSetValueWithoutOverwrite);xTaskNotifyWait(0x0,0x0,&notification, 10/portTICK_PERIOD_MS);
            }
        }
    }else if(!user_button_pressed() && hmi_input->button != RELEASED){
        hmi_input->button = RELEASED;
        if(hmi_input->status==SELECTED_OPTION){
            hmi_input->status=EXIT_OPTION;
            notify_button_status(hmi_input);
        }else if(hmi_input->status==SELECTED_MENU) notify_button_status(hmi_input);
        else if(hmi_input->status==DISPLAY_PRIMARY_MENU) notify_button_status(hmi_input);
    }
}
