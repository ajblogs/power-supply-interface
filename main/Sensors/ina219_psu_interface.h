
/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include "ina219.h"

bool INA219_connected(){
    esp_err_t ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();   
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (INA219_SENSOR_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_MASTER_INA219_PORT, cmd,  1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret != ESP_OK) {
        ESP_LOGE(INA219_TAG,"INA219 device not found!");
        return false;
        }
    return true;
}

void send_ctrl(const xTaskHandle ctrl_task,const uint32_t info){
    uint32_t notification = 0;
    bool sent = false;
    while(!sent) {
        if(xTaskNotifyWait(0,0,&notification,0)){
            if(notification == ST7789V_NOTIFY_RECEIVED) ESP_LOGI(INA219_TAG,"Cleared previous ST7789V ack notifs");
            else ESP_LOGI(INA219_TAG,"Cleared something else %x",notification);
        }
        if(xTaskNotify(ctrl_task,info,eSetValueWithoutOverwrite)){
            if(xTaskNotifyWait(0x0,0x0,&notification, 1000/portTICK_PERIOD_MS)){
                if(notification == CTRL_ACKNOWLEDGED) sent=true;
                if(notification != CTRL_ACKNOWLEDGED) ESP_LOGE(INA219_TAG,"Improper response at this time %x",notification);
            }
        }
    }
}
static float INA219_MAX_AMPERE = MAX_CURRENT_TO_SENSE;
static float INA219_MAX_VOLTAGE = MAX_VOLTAGE_TO_SENSE;
static float INA219_MAX_WATTS = MAX_POWER_TO_SENSE;

void ina219_routine(void *ina219_data_pointer){
    vTaskDelay(100/portTICK_PERIOD_MS); // Wait for all the tasks to initialize
    ina219_struct *ina219_input = (ina219_struct *)ina219_data_pointer;
    TickType_t cur_tick = xTaskGetTickCount();
    const TickType_t freq = (250/portTICK_PERIOD_MS);
    const xTaskHandle display_task = *ina219_input->display_task;
    const xTaskHandle ctrl_task = *ina219_input->ctrl_task;
    uint32_t notify;
    uint64_t cnt=0;
    volatile bool ST7789V_readiness = false;
    bool V_overload = false;
    bool A_overload = false;
    bool W_overload = false;
    bool safety = true;
    while(1){
        cnt++;
        vTaskDelayUntil(&cur_tick,freq);
        getdata_INA219(ina219_input);
        if(!ina219_input->stale){
            //Periodic self check
            if(ina219_input->Icalc > INA219_MAX_AMPERE){
                ESP_LOGE(INA219_TAG,"OverCURRENT protection kicking in! I=%fA",ina219_input->Icalc);
                if(!A_overload){
                    ST7789V_readiness = false;
                    if(safety) send_ctrl(ctrl_task,INA219_OVERCURRENT_ERROR);
                    xTaskNotify(display_task,INA219_OVERCURRENT_ERROR,eSetValueWithOverwrite);
                    A_overload = true;
                }
                //OVERCURRENT HANDLER
            }else if(ina219_input->V > INA219_MAX_VOLTAGE){
                ESP_LOGE(INA219_TAG,"OverVOLTAGE protection kicking in! V=%fV",ina219_input->V);
                if(!V_overload){
                    ST7789V_readiness = false;
                    if(safety) send_ctrl(ctrl_task,INA219_OVERVOLTAGE_ERROR);
                    xTaskNotify(display_task,INA219_OVERVOLTAGE_ERROR,eSetValueWithOverwrite);
                    V_overload = true;
                }
                //OVERVOLTAGE HANDLER - TURNOFF THE SUPPLY & DISPLAY ERROR MSG
            }else if(ina219_input->Pcalc > INA219_MAX_WATTS){
                if(!W_overload){
                    ST7789V_readiness = false;
                    ESP_LOGE(INA219_TAG,"OverWATTAGE protection kicking in! P=%fW",ina219_input->Pcalc);
                    if(safety) send_ctrl(ctrl_task,INA219_OVERWATT_ERROR);
                    xTaskNotify(display_task,INA219_OVERWATT_ERROR,eSetValueWithOverwrite);
                    W_overload = true;
                }
            }else if(V_overload){
                ESP_LOGI(INA219_TAG,"Overvoltage handled! Present Volt<%fV",INA219_MAX_VOLTAGE);
                if(ina219_input->V < INA219_MAX_VOLTAGE){
                    send_ctrl(ctrl_task,INA219_OVERVOLTAGE_ERROR_HANDLED);
                    xTaskNotify(display_task,INA219_OVERVOLTAGE_ERROR_HANDLED,eSetValueWithOverwrite);
                    V_overload = false;
                }else exit(1);
            }
            else if(A_overload){
                ESP_LOGI(INA219_TAG,"Overcurrent handled! Present Cur<%fA",INA219_MAX_AMPERE);
                if(ina219_input->Icalc < INA219_MAX_AMPERE){
                    send_ctrl(ctrl_task,INA219_OVERCURRENT_ERROR_HANDLED);
                    xTaskNotify(display_task,INA219_OVERCURRENT_ERROR_HANDLED,eSetValueWithOverwrite);
                    A_overload = false;
                }else exit(1);
            }
            else if(W_overload){
                ESP_LOGI(INA219_TAG,"Overcurrent handled! Present Pow<%fW",INA219_MAX_WATTS);
                if(ina219_input->Pcalc < INA219_MAX_WATTS){
                    send_ctrl(ctrl_task,INA219_OVERWATT_ERROR_HANDLED);
                    xTaskNotify(display_task,INA219_OVERWATT_ERROR_HANDLED,eSetValueWithOverwrite);
                    W_overload = false;
                }else exit(1);
            }
            else{              
                notify = 0;
                if(xTaskNotifyWait(0x1,0x1,&notify,2/portTICK_PERIOD_MS)){
                    if(notify==ST7789V_NOTIFY_RECEIVED){
                        ESP_LOGI(INA219_TAG,"Recieved notification %x",notify);
                    }else if(notify==ST7789V_NOTIFY_BUSY){
                        ST7789V_readiness = false;
                        ESP_LOGI(INA219_TAG,"Busy notification %x",notify);
                    }else if(notify==ST7789V_NOTIFY_AVAIL){
                        ST7789V_readiness = true;
                        ESP_LOGI(INA219_TAG,"Avail notification %x",notify);
                    }else if((notify & 0xff)==ST7789V_NOTIFY_NEW_MAX_A){
                        xTaskNotify(display_task,INA219_ACKNOWLEDGED,eSetValueWithoutOverwrite);
                        uint16_t val = (uint16_t)(notify >> 16);
                        INA219_MAX_AMPERE  = (float)((float)val/(float)100);
                        ESP_LOGI(INA219_TAG,"Change MAX A to %f (val:%d)",INA219_MAX_AMPERE,val);
                        calicon_INA219(INA219_MAX_AMPERE,INA219_CONFIGURATION_VAL);
                        // vTaskDelay(250/portTICK_PERIOD_MS);
                    }else if((notify & 0xff)==ST7789V_NOTIFY_NEW_MAX_V){
                        xTaskNotify(display_task,INA219_ACKNOWLEDGED,eSetValueWithoutOverwrite);
                        uint16_t val = notify >> 16;
                        INA219_MAX_VOLTAGE = (float)((float)val/(float)100);
                        ESP_LOGI(INA219_TAG,"Change MAX V to %f (val:%d)",INA219_MAX_VOLTAGE,val);
                    }else if((notify & 0xff)==ST7789V_NOTIFY_NEW_MAX_W){
                        xTaskNotify(display_task,INA219_ACKNOWLEDGED,eSetValueWithoutOverwrite);
                        uint16_t val = notify >> 16;
                        INA219_MAX_WATTS = (float)((float)val/(float)100);
                        ESP_LOGI(INA219_TAG,"Change MAX W to %f (val:%d)",INA219_MAX_WATTS,val);
                    }
#ifdef SAFETY_NET_OVERRIDE_MENU
                    else if(notify==INA219_SAFETY_NET_DISABLE_TOGGLE){
                        safety = !safety;
                        xTaskNotify(display_task,INA219_SAFETY_NET_DISABLE_TOGGLE,eSetValueWithOverwrite);
                        ESP_LOGE(INA219_TAG,"Safety net toggled. Excercise Caution!");
                    }
#endif
                    else{
                        ESP_LOGE(INA219_TAG,"Received unexpected notification %x",notify);
                        //RECIEVED UNEXPECTED NOTIFICATION ERROR HANDLER - SAFE_EXIT
                    }
                }else{
                        if(ST7789V_readiness) ESP_LOGE(INA219_TAG,"No notification recieved from anyone");
                        //LCD DISPLAY NOT RESPONDING ERROR HANDLER - SAFE_EXIT
                }
                if(ST7789V_readiness) xTaskNotify(display_task,INA219_NOTIFY_AVAILABLE_NEW_DATA,eSetValueWithoutOverwrite);
                else ESP_LOGI(INA219_TAG,"Valid power data !");
            }
        }
        else if (ina219_input->disconnected){
            send_ctrl(ctrl_task,INA219_DISCONNECTED);
            while(!(INA219_connected())){
                xTaskNotify(display_task,INA219_DISCONNECTED,eSetValueWithOverwrite);
                ESP_LOGE(INA219_TAG,"Trying to communicate with INA219 !");
                vTaskDelay(100/portTICK_PERIOD_MS);
            }
            send_ctrl(ctrl_task,INA219_RECONNECTED);
            xTaskNotify(display_task,INA219_RECONNECTED,eSetValueWithOverwrite);
        }
    }
}