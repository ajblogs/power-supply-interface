/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*This is the main file*/

#include "system_definitions.h"
#include "driver/gpio.h"
#include "Actuators/st7789v.h"
#include "Actuators/virtual_control.h"
#include "Sensors/ina219.h"
#include "Sensors/acs758.h"
#include "Sensors/hmi.h"

static const char* SYSTEM = "SYSTEM";

const xTaskHandle hmi_routine_handle;
const xTaskHandle ina219_routine_handle;
const xTaskHandle refresh_dispaly;
const xTaskHandle power_control_handle;

void app_main()
{
    ESP_LOGI(SYSTEM,"Begin SPI");
    lcd_struct lcd = {0xffffff,spi_setup()};
    font_struct font32 = {chr_hgt_f32,max_width_f32,firstchr_f32,widtbl_f32,chrtbl_f32,font_rle_decoder,0x0,-1,&lcd};
    lcd_fill(lcd.background ,&lcd);

    /**************/ // A task ony for updating the text

    /******************/

    graph_struct graph_s = {50,PIX_WIDTH,1,0x0,0xFFFFFF,0,33,0xFF0000,0xFFFFFF,0,85,&lcd};
    merge_graph_struct graph_settings = {71,PIX_WIDTH,1,0xff3300,0x808000,0x990033,0xFFFFFF,0xFFFFFF,lcd.background, 0,64,GRAPH_POSITIVE_VALUES_ONLY,&lcd};
    // graph_plot(20,Volt,&graph_s);
    // graph_merge_plot(-22,-25,-20,&graph_settings);

    ESP_LOGI(SYSTEM,"Begin IIC");
    ESP_ERROR_CHECK(i2c_master_init());
    calicon_INA219(MAX_CURRENT_TO_SENSE,INA219_CONFIGURATION_VAL);

    setup_user_button(USER_BUTTON_PIN);
    setup_ctrl_pin(CTRL_PIN);

    ctrl_struct ctrl_data = {&ina219_routine_handle,&hmi_routine_handle};
    ina219_struct ina219_data={}; ina219_data.display_task = &refresh_dispaly; ina219_data.ctrl_task = &power_control_handle;
    hmi_struct hmi_data = {BLANK,RELEASED,0,0,0,&refresh_dispaly,&power_control_handle};
    st7789v_struct st7789v_data = {&ina219_data,&hmi_data,&graph_s,&graph_settings,&font32,&ina219_routine_handle,&hmi_routine_handle};
    xTaskCreate(st7789v_routine, "Refreshing display upon notification", 3072, &st7789v_data, 10, &refresh_dispaly);
    xTaskCreate(ina219_routine, "access_INA219", 3072, (void *)&ina219_data, 10, &ina219_routine_handle);
    xTaskCreate(hmi_routine, "hmi_check_routine", 2048, (void *)&hmi_data, 10, &hmi_routine_handle);
    xTaskCreate(power_control_routine, "power_control_routine", 2048, (void *)&ctrl_data, 10, &power_control_handle);

    while(1){
        // vTaskSuspend(ina219_routine_handle);
        vTaskDelay(portMAX_DELAY);
    };
}
