/* 
The MIT License (MIT)

Copyright (c) 2020 ANUJ.J (anujfalcon8@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "sdkconfig.h"

/*ST7789V*/
/*Setup SPI interface for INA219*/
// #define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 19
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5
#define PIN_NUM_DC   16
#define PIN_NUM_RST  23
#define PIN_NUM_BCKL 4
/*Custom message notification values*/
#define ST7789V_NOTIFY_RECEIVED 0xA0
#define ST7789V_NOTIFY_BUSY 0xA1
#define ST7789V_NOTIFY_AVAIL 0xA2
#define ST7789V_NOTIFY_NEW_MAX_A 0xA3
#define ST7789V_NOTIFY_NEW_MAX_V 0xA4
#define ST7789V_NOTIFY_NEW_MAX_W 0xA5
#define alert_default_display_timeperiod (6000/portTICK_PERIOD_MS)

/*ACS758*/
/*Setup ADC interface for ACS758*/
#define V_REF   3000
#define ADC_CHANNEL (ADC1_CHANNEL_0)

/*INA219*/
/*Setup I2C interface for INA219*/
#define I2C_MASTER_INA219_PORT 0                        /*!< I2C port number for master dev */
#define I2C_MASTER_SDA_IO 21                            /*!< gpio number for I2C master data  */
#define I2C_MASTER_SCL_IO 22                            /*!< gpio number for I2C master clock */
#define I2C_MASTER_FREQ_HZ 1000000                      /*!< I2C master clock frequency */
/*Configuration value in accordance with  INA219 datasheet @ http://www.ti.com/lit/ds/symlink/ina219.pdf*/
#define INA219_CONFIGURATION_VAL 0b0010011111111111
/*Constant values used for calculation*/
#define INA219_VOLTAGE_LSB 0.004 // Its a constant value and should not be changed
#define INA219_MAX_SENSE_CURRENT 3 //Limitation of INA219
#define INA219_MAX_SENSE_VOLTAGE 14 //Limitation of power supply
#define INA219_MAX_SENSE_POWER ((float)INA219_MAX_SENSE_VOLTAGE*(float)INA219_MAX_SENSE_CURRENT)
#define INA219_MIN_SENSE_CURRENT 0.01 //Custom
#define INA219_MIN_SENSE_VOLTAGE 0.01 //Custom
#define INA219_MIN_SENSE_POWER ((float)INA219_MIN_SENSE_VOLTAGE*(float)INA219_MIN_SENSE_CURRENT)
#define MAX_CURRENT_TO_SENSE 1.5
#define MAX_VOLTAGE_TO_SENSE 12
#define MAX_POWER_TO_SENSE ((float)MAX_VOLTAGE_TO_SENSE*(float)MAX_CURRENT_TO_SENSE)
/*Custom message notification values*/
#define SAFETY_NET_OVERRIDE_MENU
#define INA219_ACKNOWLEDGED 0xB0
#define INA219_DISCONNECTED 0xEB1
#define INA219_RECONNECTED 0xB1
#define INA219_OVERCURRENT_ERROR 0xEB2
#define INA219_OVERVOLTAGE_ERROR 0xEB3
#define INA219_OVERWATT_ERROR 0xEB4
#define INA219_OVERCURRENT_ERROR_HANDLED 0xB2
#define INA219_OVERVOLTAGE_ERROR_HANDLED 0xB3
#define INA219_OVERWATT_ERROR_HANDLED 0xB4
#define INA219_NOTIFY_AVAILABLE_NEW_DATA 0xB5
#ifdef SAFETY_NET_OVERRIDE_MENU
    #define INA219_SAFETY_NET_DISABLE_TOGGLE 0xB6
#endif

/*CONTROL*/
/*Setup virtual control*/
#define CTRL_PIN 17
/*Custom message notification values*/
#define CTRL_ACKNOWLEDGED 0xC0
#define CTRL_NACKNOWLEDGED 0xC1

/*HMI*/
/*Setup HMI*/
#define USER_BUTTON_PIN 12
#define options_display_timeperiod (1000/portTICK_PERIOD_MS)
#define menu_display_timeperiod (1000/portTICK_PERIOD_MS)
#define primary_menu_display_timeperiod (1000/portTICK_PERIOD_MS)
#define hmi_scan_timeperiod (100/portTICK_PERIOD_MS)
/*Custom message notification values*/
#define TOGGLING_PSU_OUT 0xE // Only last 4 bits are valid. Don't use this LSB anywhere

