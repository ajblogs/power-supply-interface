# TTGO-T-Display_in_ESP-IDF

Utilizing the official development framework for ESP32 i.e. ESP-IDF for programming TTGO-T-Display module in Ubuntu

ESP-IDF: https://github.com/espressif/esp-idf<br/>
ESP-IDF installation steps: https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf<br/>
ESP-IDF API Guide : https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference

TTGO-T-Display: https://github.com/Xinyuan-LilyGO/TTGO-T-Display

The project stemmed out of the need for utilizing the module with FreeRTOS to its full potential without<br/>
being held back by the abstract layers of Arduino, so that I can actually see what I am invoking in the name<br/>
of API. The module is designed to be as a front-end for a powersupply HMI unit under design

Tools: Ubuntu 19.10, VScode, ESP32 TTGO-T-Display, Type C cable 

## Steps to run the sample on TTGO-T-Display ESP32 module 
Prerequisite: ESP-IDF should be installed. Follow the steps from official [link](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf)

- connect the module (ESP32) to computer's USB port
- `git clone https://gitlab.com/ajblogs/power-supply-interface/` 
- `cd power-supply-interface/`
- `idf.py -p /dev/ttyUSB0 monitor flash`<br/> 
*Note 1*: Please replace `/dev/ttyUSB0` with your com port value to which ESP32 is attached<br/>
*Note 2*: Steps are almost similar for other OSs. Refer the [link](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html#step-2-get-esp-idf)

## Fonts

Fonts: https://github.com/Bodmer/TFT_eSPI/tree/master/Fonts
- *Note*: Currently RLE and RAW(Max 16bits per row) format of fonts are supported.

### Steps to add a new font

- Copy the font file \*.c/\*.h to PSU/Fonts
- Add `#define max_width <max val from widtbl>` which is utilized for dynamic heap memory allocation to *.h file.
- If the font is other than RLE or RAW encoded, create your own font decoder else continue
- `#include` the *.h file in lcd.h
- Create a `font_struct` for the added font in lcd.h (The existing definitions and typename is expected to guide oneself)

Upon failure, it can be concluded that either one messed it up **or** the font file doesn't have the proper data structure *(Refer existing fonts in PSU/Fonts)*

